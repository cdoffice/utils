
err_exit() {
  exit -1;
}

gen_changelog() {
	changelogcount=30
	versionhead=`get_gitversionhead`
	git rev-list --all --pretty=format:"%h %ci %an:%s" | grep -v "^commit" | tac | nl -n ln | tac | head -${changelogcount} | sed -e "s/\(.*\)/${versionhead}.\1/g"
}

#git tag -a tag_version_1.5
#git push --tags
get_gitversionhead() {
  git_head=`git describe --match tag_version_* --long | sed -e "s|tag_version_\([0123456789.]*\)-.*|\1|g"`
  if [ "x${git_head}" == "x" ]; then
	echo "0.0"
  else
	echo "${git_head}"
  fi
}

get_gitversioncount() {
  git_commit_count=`git rev-list HEAD |wc -l`
  echo "${git_commit_count}"
}

get_gitversioninteger() {
  git_head=`git rev-parse HEAD`    #git rev-parse --short HEAD
  git_head_integer=`echo $[0x${git_head:0:7}]`
  echo "${git_head_integer}"
}

get_gitversionid() {
  git_commit_count=`get_gitversioncount`
  gitversioninteger=`get_gitversioninteger`
  echo "${git_commit_count}.${gitversioninteger}"
}

get_gitversionfull() {
  build_version_head=`get_gitversionhead`
  build_version_tail=`get_gitversionid`
  echo "${build_version_head}.${build_version_tail}"
}

get_gitversion_date_id() {
  git_date=`git log -1 --format=%ci | sed 1q | sed -e "s| ||g" -e "s|+||g" -e "s|-||g" -e "s|:||g" -e "s|...\(.\{11\}\).*|\1|g"`
  git_head=`git rev-parse HEAD`    #git rev-parse --short HEAD
  git_head_interger=`echo $[0x${git_head:0:7}]`
  echo "${git_date}" #.${git_head_interger}
}


get_git_commit_count_add() {
  git_commit_count=`git rev-list --all | wc -l| tr -d ' '`
  echo ${git_commit_count}+0$1 | bc
}

get_git_datebyte() {
  git_date=`git log -1 --format=%ci | sed 1q | sed -e "s| ||g" -e "s|+||g" -e "s|-||g" -e "s|:||g" -e "s|...\(.\{9\}\).*|\1|g"`
  echo ${git_date}
}

update_version_2_plist(){
  plistfile=$1
  version=$2
  sed -i "" "/<key>CFBundleVersion<\/key>/{n;s|\(<string>\).*\(</string>\)|\1$version\2|;}" "$plistfile"
}

update_bundleidentifier_2_plist(){
  plistfile=$1
  bundleidentifier=$2
  sed -i "" "/<key>CFBundleIdentifier<\/key>/{n;s|\(<string>\).*\(<\/string>\)|\1${bundleidentifier}\2|;}" "$plistfile"
}

update_version_2_manif(){
  plistfile=$1
  version=$2
  versionCode=$3
  sed -i "s/\(android:versionName=\"[0-9]*\.[0-9]*\.\).*\"\(.*\)/\1$version\"\2/" "$plistfile"
  sed -i "s/\(android:versionCode=\"\).*\"\(.*\)/\1$versionCode\"\2/" "$plistfile"
}

update_version_2_manif_full(){
  plistfile=$1
  version=$2
  versionCode=$3
  sed -i "s/\(android:versionName=\"\).*\"\(.*\)/\1$version\"\2/" "$plistfile"
  sed -i "s/\(android:versionCode=\"\).*\"\(.*\)/\1$versionCode\"\2/" "$plistfile"
}

ppfile_get_uuid() {
  #0EB3B550-0BF6-4297-AB6B-EACAB1F9B0D3
  ppfile=$1
  grep -a "<string>.*-.*-.*-.*-.*</string>" "$ppfile" | sed -e "s|.*<string>\(.*\)</string>|\1|"
}

echo_ios_ota_plist() {

releaseprojname=$1
appfile=$2
appid=$3
appname=$4
base_url=$5

cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>items</key>
  <array>
    <dict>
      <key>assets</key>
      <array>
        <dict>
          <key>kind</key>
          <string>software-package</string>
          <key>url</key>
          <string>${base_url}/${releaseprojname}/${appfile}</string>
        </dict>
        <!-- display-image: the icon to display during download. -->
        <dict>
            <key>kind</key>
            <string>display-image</string>
            <!-- optional. icon needs shine effect applied. -->
            <key>needs-shine</key>
            <true/>
            <key>url</key>
            <string>${base_url}/RCS_AppIcon_57.png</string>
        </dict>
        <!-- full-size-image: the large 512Â¡Ã512 icon used by iTunes. -->
        <dict>
                <key>kind</key>
                <string>full-size-image</string>
                <!-- optional.  one md5 hash for the entire file. -->
                <key>md5</key>
                <string>d7da99f613e10c8285a1d291cfc7554b </string>
                <key>needs-shine</key>
                <true/>
                <key>url</key>
                <string>${base_url}/RCS_AppIcon_512.png</string>
        </dict>
      </array>
      <key>metadata</key>
      <dict>
        <key>bundle-identifier</key>
        <string>${appid}</string>
        <key>kind</key>
        <string>software</string>
        <key>title</key>
        <string>${appname}</string>
      </dict>
    </dict>
  </array>
</dict>
</plist>
EOF

}

