#!/bin/sh
set -x
rm -f bashlib_dev.inc
wget xxx/bashlib_dev.inc
source ./bashlib_dev.inc

err_exit() {
  exit -1;
}
##many depend packages are needed
# yum install -y make ncurses-devel.x86_64 openssl.x86_64 openssl-devel.x86_64 gcc gcc-c++.x86_64 java-1.6.0-openjdk.x86_64 java-1.6.0-openjdk-devel.x86_64
#yum install ncurses-devel unixODBC-devel.x86_64
export PKG_CONFIG_PATH=/lib64/pkgconfig/

echo "#############################################################"
echo "           Starting install erlang( OTP_R15B03)                       "
echo "#############################################################"

ERL_TAG=`git describe`
ERL_DIR="erlang_$ERL_TAG"
ERL_INSTALL=`pwd`/buildout/$ERL_DIR

echo "###########make install erlang###############################"
rm -rf buildout/
rm -rf bootstrap erts lib make plt system xcomp autom4te.cache
git reset --hard HEAD
mkdir -p buildout/all buildout/release || err_exit
make clean
./otp_build remove_prebuilt_files
./otp_build save_bootstrap

if true; then
 ./otp_build setup --prefix=$ERL_INSTALL || err_exit
 ./otp_build release -a $ERL_INSTALL  || err_exit
else
 ./otp_build autoconf || err_exit
 ./configure --prefix=$ERL_INSTALL || err_exit
 make || err_exit
 make install  || err_exit
fi
echo `get_gitversionfull > $ERL_INSTALL/grversion.txt` || err_exit

uploadname="voip/server"
outputfile=`pwd`/buildout/all/$ERL_DIR-`get_gitversionfull`-`date "+%Y%m%d%H%M%S"`.tar.bz2
tar cjvf $outputfile -C `pwd`/buildout  $ERL_DIR  || err_exit

curl -k -i --max-time 300 -F path=${uploadname} -F file=@${outputfile} https://xxxxx:8000/xxx/xxx/

