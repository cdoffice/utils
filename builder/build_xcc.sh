#!/bin/sh
set -x
rm -f bashlib_dev.inc
wget http://xxxx/bashlib_dev.inc
source ./bashlib_dev.inc

err_exit() {
  exit -1;
}
##many depend packages are needed
# yum install -y make ncurses-devel.x86_64 openssl.x86_64 openssl-devel.x86_64 gcc gcc-c++.x86_64 java-1.6.0-openjdk.x86_64 java-1.6.0-openjdk-devel.x86_64
#yum install ncurses-devel unixODBC-devel.x86_64
export PKG_CONFIG_PATH=/lib64/pkgconfig/

echo "#############################################################"
echo "           Starting Build xcc                              "
echo "#############################################################"

RELEASE_TAG=`git describe`
RELEASE_DIR="xcc"
RELEASE_INSTALL=`pwd`/buildout/$RELEASE_DIR

echo "###########make install erlang###############################"
rm -rf buildout/
git reset --hard HEAD
mkdir -p buildout/all buildout/release $RELEASE_INSTALL/ || err_exit
cp -a `ls | grep -v buildout | grep -v build_xcc.sh` $RELEASE_INSTALL/

get_gitversionfull > $RELEASE_INSTALL/version.txt || err_exit
git describe >> $RELEASE_INSTALL/version.txt || err_exit

uploadname="voip/server/xcc"
outputfile=`pwd`/buildout/all/$RELEASE_DIR-$RELEASE_TAG-`get_gitversionfull`-`date "+%Y%m%d%H%M%S"`.tar.bz2
tar cjvf $outputfile -C `pwd`/buildout  $RELEASE_DIR  || err_exit

curl -k -i --max-time 300 -F path=${uploadname} -F file=@${outputfile} https://xxxx/upload/

